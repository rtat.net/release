# GitLab Security Products QA process

To ensure we deliver features matching quality requirements, each release goes through a QA phase, explained below.

## Automated QA

To ease the QA task, the Release Manager should use the [QA trigger](../scripts/qa_trigger.rb) script that will automatically trigger a pipeline on each [test project](https://gitlab.com/gitlab-org/security-products/tests).

### Usage

Run:

```shell
./scripts/qa_trigger.rb
```

By default this will trigger a pipeline on the `QA-all-base-FREEZE` branch but you can also specify a different one with:


```shell
./scripts/qa_trigger.rb another-qa-branch
```

### How it works?

Each test project has a `QA-all-base-FREEZE` branch with all the (compatible) Security Products features set up.
This branch also have a `qa-xxx` job configured for each corresponding Security Products feature that is set up and that
will compare the effective output with an expected one stored in the test project.

The QA job will fail if files don't match.

### How to add a project to the automated QA

You first need to setup a correct Security Products project by [following the documentation](https://gitlab.com/gitlab-org/security-products/tests/common#how-to-create-a-new-test-project).

Then you need to add a new entry to the [`test_projects.yml`](../scripts/test_projects.yml) file with the corresponding pipeline trigger token.

## Manual QA

On top of the automated QA that ensures no regression has been introduced, the Release Manager needs to pay attention to the new features added in the iteration.
The [qa_issue.rb](../scripts/qa_issue.rb) script will generate an issue description containing all the changelogs of the configured projects to help the Release Manager achieving that.

### How to add a project to the Manual QA

- add a new entry to the [`projects.yml`](../scripts/projects.yml) file.
