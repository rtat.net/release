# Secure - Release Managers

This page lists the Release Managers for the Secure team.

- `11.6`: @fcatteau
- `11.5`: @groulot
- `11.4`: @gonzoyumo
- `11.3`: @fcatteau
- `11.2`: @groulot
- `11.1`: @fcatteau
- `11.0`: @fcatteau
- `10.8`: @gonzoyumo
- `10.7`: @groulot
- `10.6`: @fcatteau
- `10.5`: @gonzoyumo
