# frozen_string_literal: true

require 'git'
require 'versionomy'
require 'yaml'

# Utility module to load projects
module Projects
  CLONE_DIR = if !ENV['GOPATH'].nil?
                File.join(ENV['GOPATH'], 'src', 'gitlab.com/gitlab-org/security-products').freeze
              else
                'tmp'
              end

  # Format a version object into the "X-Y-stable" format
  def self.branch_name_for_version(a_version)
    "#{a_version.major}-#{a_version.minor}-stable"
  end

  # Basic representation of a project with associated repo and utility methods
  class Project
    attr_reader :name
    attr_reader :namespace
    attr_reader :path
    attr_reader :repository

    def initialize(namespace, name, repository)
      @namespace = namespace
      @name = name
      @repository = repository
      @path = File.join([@namespace, @name].compact)
      @clone_path = File.join(CLONE_DIR, @path)
    end

    def web_url
      # TODO
      # git@gitlab.com:gitlab-org/security-products/analyzers/bandit.git
      # https://gitlab.com/gitlab-org/security-products/analyzers/bandit
      repository.sub(':', '/').sub(/^git@/, 'https://').sub(/.git$/, '')
    end

    def file_url(path, ref)
      "#{web_url}/blob/#{ref}/#{path}"
    end

    def clone_or_update_repo
      unless File.directory?(@clone_path)
        # We need to clone the repository
        print "Cloning repository #{@repository}... "
        Git.clone(@repository, @clone_path, depth: 1)
        puts 'done.'
      end

      # Fetch remote changes
      git.fetch
    end

    def checkout_branch(branch)
      if git.branches.any? { |b| b.name == branch && b.remote.nil? }
        update_existing_branch(branch)
      elsif git.branches.any? { |b| b.name == branch }
        checkout_branch_from_origin(branch)
      else
        create_new_branch(branch)
      end
    end

    # Read version from VERSION file
    def version
      content = File.read(File.join(@clone_path, 'VERSION')).strip
      major, minor, = content.split('-')
      Versionomy.create(major: major, minor: minor)
    end

    def check_version_file(wanted_version)
      # Compare with the version found in the VERSION file
      raise("Wrong version for #{@clone_path}: got #{version}") if version != wanted_version
    end

    def changelog_for_version(wanted_version)
      # Read anything between "## X-Y-stable" and previous release
      # (line starting with "##")
      wanted_branch_name = Projects.branch_name_for_version(wanted_version)
      content = changelog.match(/^## #{wanted_branch_name}\n((?:(?!(?:^##)).)*)/m)[1]
                         .gsub(/^\n/, '') # remove empty lines

      # preprend lines with a checkbox
      content.empty? ? 'Nothing' : content.gsub(/^(?:- )?(.*)/, '- [ ] \1')
    end

    def bump(new_version)
      new_branch = Projects.branch_name_for_version(new_version)

      # Checkout the master branch
      checkout_branch('master')

      if git.log(100).any? { |log| log.message == "Bump version #{new_version}" }
        # Already done
        puts 'Already bumped.'
        return
      end

      # Bump files
      git.commit("Bump version #{new_version}") if [bump_version(new_branch), bump_changelog(new_branch)].any?
    end

    def push
      git.push('origin', git.current_branch)
    end

    private

    def changelog
      @changelog ||= File.read(changelog_path)
    end

    def git
      @git ||= Git.open(@clone_path)
    end

    def version_path
      File.join(@clone_path, 'VERSION')
    end

    def changelog_path
      File.join(@clone_path, 'CHANGELOG.md')
    end

    def update_existing_branch(branch)
      # Update our existing branch from origin
      print "Updating branch #{branch}... "
      git.checkout(branch)
      git.pull
      puts 'done.'
    end

    def checkout_branch_from_origin(branch)
      # It exists on origin, check it out
      print "Getting #{branch} from origin... "
      git.checkout(branch)
      git.pull
      puts 'done.'
    end

    def create_new_branch(branch)
      # Create the new branch from master
      print "Creating a new branch #{branch} for #{@repository}... "
      git.checkout('master')
      git.pull
      git.branch(branch).checkout
      puts 'done.'
    end

    def bump_version(new_branch)
      # Update VERSION file content
      current_branch = File.read(version_path).strip

      return false unless current_branch != new_branch

      # We need to update the VERSION file
      File.write(version_path, new_branch)
      puts version_path.to_s
      git.add('VERSION')
      true
    end

    def bump_changelog(new_branch)
      current_changelog = File.read(changelog_path)

      return false unless current_changelog.lines.grep(/#{new_branch}/).empty?

      # We need to add an entry to the changelog for version
      first_version_line_found = false

      new_changelog = current_changelog.lines.map do |line|
        if /## \d+-\d+-stable/ =~ line && !first_version_line_found
          # We found the first version line in the changelog. Prepend the new one
          first_version_line_found = true
          "## #{new_branch}\n\n#{line}"
        else
          line
        end
      end

      abort("Changelog doesn't have any version entry #{changelog_path}") unless first_version_line_found
      @changelog = new_changelog.join
      File.write(changelog_path, @changelog)
      git.add('CHANGELOG.md')
      true
    end
  end

  # Load projects, cloning them if needed
  def self.load
    projects = YAML.load_file(File.expand_path('./projects.yml', __dir__))

    projects.map do |project|
      Project.new(project['namespace'], project['name'], project['repo_url'])
    end
  end

  def self.bump(projects, version)
    version = Versionomy.parse(version)

    projects.each { |project| project.bump(version) }
  end
end
