# frozen_string_literal: true

require 'gitlab'
require 'versionomy'
require 'yaml'

module Gitlab
  class Client
    # MergeRequests helper
    module MergeRequests
      def group_merge_requests(group = nil, options = {})
        get("/groups/#{url_encode group}/merge_requests", query: options)
      end
    end
  end
end

# Utility module to load Secure merge requests for a release
module MergeRequests
  # Represents a merge request
  class MergeRequest
    attr_reader :title
    attr_reader :url

    def initialize(title, url)
      @title = title
      @url = url
    end
  end

  def self.configure
    Gitlab.configure do |config|
      config.endpoint       = 'https://gitlab.com/api/v4'
      config.private_token  = ENV['GITLAB_API_PRIVATE_TOKEN']
    end
  end

  def self.load(milestone)
    configure

    Gitlab.group_merge_requests(
      'gitlab-org',
      labels: 'Secure',
      milestone: milestone.to_s,
      state: 'merged',
      per_page: 999
    )
  end
end
